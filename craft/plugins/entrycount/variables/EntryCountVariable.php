<?php
namespace Craft;

/**
 * Entry Count Variable
 */
class EntryCountVariable
{
    /**
     * Returns count
     *
	 * @param int $entryId
	 *
	 * @return EntryCountModel
     */
    public function getCount($entryId)
    {
        return craft()->entryCount->getCount($entryId);
    }

    /**
     * Returns counted entries
     *
     * @return ElementCriteriaModel
     */
    public function getEntries()
    {
        return craft()->entryCount->getEntries();
    }

    /**
     * Increment count
     *
	 * @param int $entryId
     */
    public function increment($entryId)
    {
        craft()->entryCount->increment($entryId);
    }
    public function sortPopular($array)
    {
        $array = $array->find();
        function cmp($a, $b){
          $a_count = EntryCountVariable::getCount( $a->id );
          $b_count = EntryCountVariable::getCount( $b->id );
          if ($a_count == $b_count) {
            return 0;
          }
          return ($a_count > $b_count) ? -1 : 1;
        }
        @usort($array, 'Craft\cmp');
        return $array;
    }
}
