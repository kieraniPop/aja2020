<?php
namespace Craft;

class Eventbrite_BaseService extends BaseApplicationComponent
{
	// Blank variables
	protected $oAuthToken;
	public $cacheDuration;
	public $orgID;

	// Called when clicking any page on craft
	public function init()
	{
		// Gets settings from back end of craft
		$settings = craft()->plugins->getPlugin('eventbrite')->getSettings();

		// Sets previously blank variables to relevant settings
		$this->oAuthToken = $settings->oAuthToken;
		$this->cacheDuration = $settings->cacheDuration;
		$this->orgID = $settings->orgID;
	}

	protected function _makeRequest($url)
	{
		try {
			$cached = craft()->cache->get($url);
			if ($cached) {
				return $cached;
			} else {
				$client = new \Guzzle\Http\Client();
				$request = $client->get($url);
				$request->addHeader('Authorization', 'Bearer ' . $this->oAuthToken);
				$response = $request->send();
				if (!$response->issuccessful()){
					return;
				}
				$output = $response->json();
				craft()->cache->set($url, $output, $this->cacheDuration);
				return $output;
			}
		} catch(\Exception $e) {
			throw $e;
		}
	}

	protected function _buildEventbriteUrl($endpoint)
	{
		// https://www.eventbriteapi.com/v3/
		// In example: https://www.eventbriteapi.com/v3/venues/$id?
		return 'https://www.eventbriteapi.com/v3/' . $endpoint . '?';
	}

	protected function _getIdFromOptions(&$options)
	{
		return $this->pop_from_array($options, 'id');
	}

  protected function _get($endpoint, $options){
		// endpoint passed in example = venues /$id/
		// Options - id: venues + $id /
    $query    = http_build_query($options);
		// endpoint passed in example = venues /$id/
		// Returned: https://www.eventbriteapi.com/v3/venues/48270341?
		// $url = https://www.eventbriteapi.com/v3/venues/$id?$query
		// venue example - 48270341
    $url      = $this->_buildEventbriteUrl($endpoint) . $query;

    $response = $this->_makeRequest($url);
    return $response;
  }

	public function pop_from_array(&$array, $key, $default = null)
	{
		if (array_key_exists($key, $array)) {
			$val = $array[$key];
			unset($array[$key]);
			return $val;
		} else {
			return $default;
		}
	}
}
