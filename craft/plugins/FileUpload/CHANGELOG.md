# File Upload Changelog

## 0.0.1 -- 2018-02-19

* Initial release

Brought to you by [Luca Jegard](github.com/jegard)
