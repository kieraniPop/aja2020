<?php
namespace Craft;

class CocktailRecipesVariable
{
    public function findIngredients($criteria)
    {
        $criteria = new CocktailRecipes_IngredientCriteria($criteria);
        return craft()->cocktailRecipes->findIngredients($criteria);
    }

    public function findIngredient($criteria)
    {
        $criteria = new CocktailRecipes_IngredientCriteria($criteria);
        return craft()->cocktailRecipes->findIngredient($criteria);
    }
}
