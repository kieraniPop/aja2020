<?php
/**
 * File Upload plugin for Craft CMS
 *
 * FileUpload Controller
 *
 * --snip--
 * Generally speaking, controllers are the middlemen between the front end of the CP/website and your plugin’s
 * services. They contain action methods which handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering post data, saving it on a model,
 * passing the model off to a service, and then responding to the request appropriately depending on the service
 * method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what the method does (for example,
 * actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 * --snip--
 *
 * @author    Luca Jegard
 * @copyright Copyright (c) 2018 Luca Jegard
 * @link      github.com/jegard
 * @package   FileUpload
 * @since     0.0.1
 */

namespace Craft;

class FileUploadController extends BaseController
{

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     * @access protected
     */
    protected $allowAnonymous = array('actionIndex',
        );

    /**
     * Handle a request going to our plugin's index action URL, e.g.: actions/fileUpload
     */
    public function actionIndex()
    {
    }
    protected $valid_extensions = array(
      // images
      'jpg',
      'jpeg',
      'png',
      // archives
      'zip',
      'rar',
      // video
      'mp4',
      'avi',
      // document
      'doc',
      'docx',
      'odt',
      'pdf',
      'rtf',
      'tex',
      'txt',
      'wks'
    );
    protected $assetSourceId = 1;

    public function actionInitial()
    {
        file_put_contents( __DIR__ . '/session', $_POST );
        $this->requireAjaxRequest();
        $this->returnJson(
        [
        array('name' => 'nam1','uuid' => '234')
        ]
      );
    }
    public function actionDelete()
    {
        $this->requireAjaxRequest();
        //file_put_contents(__DIR__ . '/test2.json', json_encode($_POST));
        $this->returnJson(array('success' => true));
    }
    public function actionUpload()
    {
        $this->requireAjaxRequest();
        $errors = array();
        $success = array();
        file_put_contents(__DIR__ . '/test.json', json_encode($_FILES['qqfile']));

        $type = $_FILES['qqfile']['type'];

        $path = $_FILES['qqfile']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        if((
          $ext == 'png' ||
          $ext == 'jpeg' ||
          $ext == 'jpg'

          ) && !$_FILES['qqfile']['error'] ){
          //no intial error
          $uploadDir = craft()->assetSources->getSourceById(1)->settings['path'];
          $filename = $_FILES['qqfile']['name'];
          $extensionArr = explode('.', $filename);
          //$extension = $extensionArr[ count($extensionArr) ];
          $file = $_FILES['qqfile']['tmp_name'];
          //substr(md5(rand()), 0, 7)

          $image_info = getimagesize($_FILES['qqfile']['tmp_name']);

          $image_width = $image_info[0];
          $image_height = $image_info[1];

          $hash = substr(md5(rand()), 0, 7);
          if(move_uploaded_file($file, $uploadDir . $hash . '-' . $filename)){
            IOHelper::deleteFile($file);
            $file = $uploadDir . $hash . '-' . $filename;
            $fileModel = new AssetFileModel();
            $fileModel->sourceId = 1;
            $fileModel->folderId = 1;
            $fileModel->filename = IOHelper::getFileName($uploadDir . $hash . '-' . $filename);

            $fileModel->dateModified = time();
            $fileModel->dateCreated = time();
            $fileModel->dateUpdated = time();

            $fileModel->width = $image_width;
            $fileModel->height = $image_height;

            $fileModel->kind = IOHelper::getFileKind(IOHelper::getExtension($filename));
            $fileModel->size = filesize($file);
            craft()->assets->storeFile($fileModel);
            $criteria = craft()->elements->getCriteria(ElementType::Asset);
            $criteria->filename = $fileModel->filename;
            $asset = $criteria->find()[0];

            $this->returnJson(array(
              'success' => true,
              'assetId' => $asset->id
            ));
          }else{
            $this->returnJson(array('success' => false));
          }
        }else{
          //Error
          $this->returnJson(array('success' => false));
        }

    }
    public function actionUploadAny()
    {
        $this->requireAjaxRequest();
        $errors = array();
        $success = array();
        file_put_contents(__DIR__ . '/test.json', json_encode($_FILES['qqfile']));
        $type = $_FILES['qqfile']['type'];


        $path = $_FILES['qqfile']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);

        file_put_contents(__DIR__ . '/mime.json', json_encode($ext));

        if( (
          $ext == 'jpg' ||
          $ext == 'jpeg' ||
          $ext == 'png' ||
          $ext == 'zip' ||
          $ext == 'rar' ||
          $ext == 'mp4' ||
          $ext == 'avi' ||
          $ext == 'doc' ||
          $ext == 'docx' ||
          $ext == 'odt' ||
          $ext == 'pdf' ||
          $ext == 'rtf' ||
          $ext == 'tex' ||
          $ext == 'txt' ||
          $ext == 'wks' ||
          $ext == 'pptx' ||
          $ext == 'pptm' ||
          $ext == 'ppt' ||
          $ext == 'xlsx' ||
          $ext == 'xlsm' ||
          $ext == 'xlsx' ||
          $ext == 'xltx' ||
          $ext == 'xltm'
          ) && !$_FILES['qqfile']['error'] ){
          //no intial error
          $uploadDir = craft()->assetSources->getSourceById(1)->settings['path'];
          $filename = $_FILES['qqfile']['name'];
          $extensionArr = explode('.', $filename);
          //$extension = $extensionArr[ count($extensionArr) ];
          $file = $_FILES['qqfile']['tmp_name'];
          //substr(md5(rand()), 0, 7)

          $image_info = getimagesize($_FILES['qqfile']['tmp_name']);

          $image_width = $image_info[0];
          $image_height = $image_info[1];

          $hash = substr(md5(rand()), 0, 7);
          if(move_uploaded_file($file, $uploadDir . $hash . '-' . $filename)){
            IOHelper::deleteFile($file);
            $file = $uploadDir . $hash . '-' . $filename;
            $fileModel = new AssetFileModel();
            $fileModel->sourceId = 1;
            $fileModel->folderId = 1;
            $fileModel->filename = IOHelper::getFileName($uploadDir . $hash . '-' . $filename);

            $fileModel->dateModified = time();
            $fileModel->dateCreated = time();
            $fileModel->dateUpdated = time();

            $fileModel->width = $image_width;
            $fileModel->height = $image_height;

            $fileModel->kind = IOHelper::getFileKind(IOHelper::getExtension($filename));
            $fileModel->size = filesize($file);
            craft()->assets->storeFile($fileModel);
            $criteria = craft()->elements->getCriteria(ElementType::Asset);
            $criteria->filename = $fileModel->filename;
            $asset = $criteria->find()[0];

            $this->returnJson(array(
              'success' => true,
              'assetId' => $asset->id
            ));
          }else{
            $this->returnJson(array('success' => false));
          }
        }else{
          //Error
          $this->returnJson(array('success' => false));
        }

    }
}
