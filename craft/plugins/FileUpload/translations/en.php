<?php
/**
 * File Upload plugin for Craft CMS
 *
 * File Upload Translation
 *
 * @author    Luca Jegard
 * @copyright Copyright (c) 2018 Luca Jegard
 * @link      github.com/jegard
 * @package   FileUpload
 * @since     0.0.1
 */

return array(
    'Translate me' => 'To this',
);
