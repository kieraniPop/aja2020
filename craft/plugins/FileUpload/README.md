# File Upload plugin for Craft CMS

A ajax file upload controller

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install File Upload, follow these steps:

1. Download & unzip the file and place the `fileupload` directory into your `craft/plugins` directory
2.  -OR- do a `git clone https://github.com/jegard/fileupload.git` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require jegard/fileupload`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `fileupload` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

File Upload works on Craft 2.4.x and Craft 2.5.x.

## File Upload Overview

-Insert text here-

## Configuring File Upload

-Insert text here-

## Using File Upload

-Insert text here-

## File Upload Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [Luca Jegard](github.com/jegard)
