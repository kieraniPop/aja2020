$(document).ready(function(){
    // video pop up 
  
    var Swipers = new Swiper('.swiper-container2', {
      loop: true,
      // autoplay: {
      //     delay: 5000,
      //   },
      navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
      },
      pagination: {
          el: '.swiper-pagination',
      },
    });

    function openModal(modal, button){
      // Setting the modal contents...
      $(modal).fadeIn("slow");
  
      if(button){
          var src = $(button).attr('data-video');
          $(modal + ' iframe').attr('src', src);
      }
    }
  
    $('.swiper-slide').click(function(e){
      e.preventDefault();
    })
  
    $('.js-open-video').click(function(e){
      e.preventDefault();
      openModal('#openVideo', $(this));
    });

    $('.js-open-video2').click(function(e){
      e.preventDefault();
      openModal('#openVideo2', $(this));
    });
  
    // When user clicks outside of modal, close it or when they click close
    $('.close-modal').click(function(){
      $('.modal').fadeOut();
      $('.openVideo iframe').attr('src', '');
    });
    $('.modal').click(function(){
      $('.modal').fadeOut();
      $('.openVideo iframe').attr('src', '');
    });

    // Prevent user clicking contents of modal and accidentally closing the modal
    $('.modal-content').click(function(e){
      e.stopPropagation();
    });



  // Accordion
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
      console.log("test");
      acc[i].addEventListener("click", function() {
          console.log("clicked");
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.maxHeight){
              panel.style.maxHeight = null;
          } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
          }
      });
  }

  // even heights div

  function setEvenHeights(){
    $('.js-even-heights').each(function(){
        var height = 0;
        var height2 = 0;

        $(this).find('.js-set').each(function(){
            $(this).css('height','auto');
            if ($(this).outerHeight() > height){
                height = $(this).outerHeight();
            }
        });

        $(this).find('.js-set2').each(function(){
            $(this).css('height','auto');
            if ($(this).outerHeight() > height2){
                height2 = $(this).outerHeight();
            }
        });
        $('.js-set', this).outerHeight(height);
        $('.js-set2', this).outerHeight(height2);
    });
  };

  setEvenHeights();

  $(window).resize(function () {
      setEvenHeights();
  });

  $('.js-select').click(function(){
    $('.js-select').removeClass('active');
    $('.tab').hide();
    $(this).addClass('active');
    $('#'+$(this).attr('data-tab')).show();
    setEvenHeights();
  });
  $('.js-select').click(function(){
    $('.js-select').removeClass('active');
    $('.tab').hide();
    $(this).addClass('active');
    $('#'+$(this).attr('data-tab')).show();
    setEvenHeights();
  });
});