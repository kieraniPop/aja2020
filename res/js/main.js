/*!
Waypoints - 4.0.1
Copyright © 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
!function(){"use strict";function t(n){if(!n)throw new Error("No options passed to Waypoint constructor");if(!n.element)throw new Error("No element option passed to Waypoint constructor");if(!n.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+e,this.options=t.Adapter.extend({},t.defaults,n),this.element=this.options.element,this.adapter=new t.Adapter(this.element),this.callback=n.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=t.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=t.Context.findOrCreateByElement(this.options.context),t.offsetAliases[this.options.offset]&&(this.options.offset=t.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),i[this.key]=this,e+=1}var e=0,i={};t.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},t.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},t.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete i[this.key]},t.prototype.disable=function(){return this.enabled=!1,this},t.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},t.prototype.next=function(){return this.group.next(this)},t.prototype.previous=function(){return this.group.previous(this)},t.invokeAll=function(t){var e=[];for(var n in i)e.push(i[n]);for(var o=0,r=e.length;r>o;o++)e[o][t]()},t.destroyAll=function(){t.invokeAll("destroy")},t.disableAll=function(){t.invokeAll("disable")},t.enableAll=function(){t.Context.refreshAll();for(var e in i)i[e].enabled=!0;return this},t.refreshAll=function(){t.Context.refreshAll()},t.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},t.viewportWidth=function(){return document.documentElement.clientWidth},t.adapters=[],t.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},t.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=t}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}function e(t){this.element=t,this.Adapter=o.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+i,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,n[t.waypointContextKey]=this,i+=1,o.windowContext||(o.windowContext=!0,o.windowContext=new e(window)),this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var i=0,n={},o=window.Waypoint,r=window.onload;e.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},e.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical),i=this.element==this.element.window;t&&e&&!i&&(this.adapter.off(".waypoints"),delete n[this.key])},e.prototype.createThrottledResizeHandler=function(){function t(){e.handleResize(),e.didResize=!1}var e=this;this.adapter.on("resize.waypoints",function(){e.didResize||(e.didResize=!0,o.requestAnimationFrame(t))})},e.prototype.createThrottledScrollHandler=function(){function t(){e.handleScroll(),e.didScroll=!1}var e=this;this.adapter.on("scroll.waypoints",function(){(!e.didScroll||o.isTouch)&&(e.didScroll=!0,o.requestAnimationFrame(t))})},e.prototype.handleResize=function(){o.Context.refreshAll()},e.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var n=e[i],o=n.newScroll>n.oldScroll,r=o?n.forward:n.backward;for(var s in this.waypoints[i]){var l=this.waypoints[i][s];if(null!==l.triggerPoint){var a=n.oldScroll<l.triggerPoint,h=n.newScroll>=l.triggerPoint,p=a&&h,u=!a&&!h;(p||u)&&(l.queueTrigger(r),t[l.group.id]=l.group)}}}for(var d in t)t[d].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},e.prototype.innerHeight=function(){return this.element==this.element.window?o.viewportHeight():this.adapter.innerHeight()},e.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},e.prototype.innerWidth=function(){return this.element==this.element.window?o.viewportWidth():this.adapter.innerWidth()},e.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var n=0,o=t.length;o>n;n++)t[n].destroy()},e.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),n={};this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var r in t){var s=t[r];for(var l in this.waypoints[r]){var a,h,p,u,d,f=this.waypoints[r][l],c=f.options.offset,w=f.triggerPoint,y=0,g=null==w;f.element!==f.element.window&&(y=f.adapter.offset()[s.offsetProp]),"function"==typeof c?c=c.apply(f):"string"==typeof c&&(c=parseFloat(c),f.options.offset.indexOf("%")>-1&&(c=Math.ceil(s.contextDimension*c/100))),a=s.contextScroll-s.contextOffset,f.triggerPoint=Math.floor(y+a-c),h=w<s.oldScroll,p=f.triggerPoint>=s.oldScroll,u=h&&p,d=!h&&!p,!g&&u?(f.queueTrigger(s.backward),n[f.group.id]=f.group):!g&&d?(f.queueTrigger(s.forward),n[f.group.id]=f.group):g&&s.oldScroll>=f.triggerPoint&&(f.queueTrigger(s.forward),n[f.group.id]=f.group)}}return o.requestAnimationFrame(function(){for(var t in n)n[t].flushTriggers()}),this},e.findOrCreateByElement=function(t){return e.findByElement(t)||new e(t)},e.refreshAll=function(){for(var t in n)n[t].refresh()},e.findByElement=function(t){return n[t.waypointContextKey]},window.onload=function(){r&&r(),e.refreshAll()},o.requestAnimationFrame=function(e){var i=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t;i.call(window,e)},o.Context=e}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}function i(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),n[this.axis][this.name]=this}var n={vertical:{},horizontal:{}},o=window.Waypoint;i.prototype.add=function(t){this.waypoints.push(t)},i.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},i.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var n=this.triggerQueues[i],o="up"===i||"left"===i;n.sort(o?e:t);for(var r=0,s=n.length;s>r;r+=1){var l=n[r];(l.options.continuous||r===n.length-1)&&l.trigger([i])}}this.clearTriggerQueues()},i.prototype.next=function(e){this.waypoints.sort(t);var i=o.Adapter.inArray(e,this.waypoints),n=i===this.waypoints.length-1;return n?null:this.waypoints[i+1]},i.prototype.previous=function(e){this.waypoints.sort(t);var i=o.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},i.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},i.prototype.remove=function(t){var e=o.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},i.prototype.first=function(){return this.waypoints[0]},i.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},i.findOrCreate=function(t){return n[t.axis][t.name]||new i(t)},o.Group=i}(),function(){"use strict";function t(t){return t===t.window}function e(e){return t(e)?e:e.defaultView}function i(t){this.element=t,this.handlers={}}var n=window.Waypoint;i.prototype.innerHeight=function(){var e=t(this.element);return e?this.element.innerHeight:this.element.clientHeight},i.prototype.innerWidth=function(){var e=t(this.element);return e?this.element.innerWidth:this.element.clientWidth},i.prototype.off=function(t,e){function i(t,e,i){for(var n=0,o=e.length-1;o>n;n++){var r=e[n];i&&i!==r||t.removeEventListener(r)}}var n=t.split("."),o=n[0],r=n[1],s=this.element;if(r&&this.handlers[r]&&o)i(s,this.handlers[r][o],e),this.handlers[r][o]=[];else if(o)for(var l in this.handlers)i(s,this.handlers[l][o]||[],e),this.handlers[l][o]=[];else if(r&&this.handlers[r]){for(var a in this.handlers[r])i(s,this.handlers[r][a],e);this.handlers[r]={}}},i.prototype.offset=function(){if(!this.element.ownerDocument)return null;var t=this.element.ownerDocument.documentElement,i=e(this.element.ownerDocument),n={top:0,left:0};return this.element.getBoundingClientRect&&(n=this.element.getBoundingClientRect()),{top:n.top+i.pageYOffset-t.clientTop,left:n.left+i.pageXOffset-t.clientLeft}},i.prototype.on=function(t,e){var i=t.split("."),n=i[0],o=i[1]||"__default",r=this.handlers[o]=this.handlers[o]||{},s=r[n]=r[n]||[];s.push(e),this.element.addEventListener(n,e)},i.prototype.outerHeight=function(e){var i,n=this.innerHeight();return e&&!t(this.element)&&(i=window.getComputedStyle(this.element),n+=parseInt(i.marginTop,10),n+=parseInt(i.marginBottom,10)),n},i.prototype.outerWidth=function(e){var i,n=this.innerWidth();return e&&!t(this.element)&&(i=window.getComputedStyle(this.element),n+=parseInt(i.marginLeft,10),n+=parseInt(i.marginRight,10)),n},i.prototype.scrollLeft=function(){var t=e(this.element);return t?t.pageXOffset:this.element.scrollLeft},i.prototype.scrollTop=function(){var t=e(this.element);return t?t.pageYOffset:this.element.scrollTop},i.extend=function(){function t(t,e){if("object"==typeof t&&"object"==typeof e)for(var i in e)e.hasOwnProperty(i)&&(t[i]=e[i]);return t}for(var e=Array.prototype.slice.call(arguments),i=1,n=e.length;n>i;i++)t(e[0],e[i]);return e[0]},i.inArray=function(t,e,i){return null==e?-1:e.indexOf(t,i)},i.isEmptyObject=function(t){for(var e in t)return!1;return!0},n.adapters.push({name:"noframework",Adapter:i}),n.Adapter=i}();

"use strict";

Vue.options.delimiters = ['${', '}'];

Vue.use(VueLazyload);

// In a plugin, this would be defined
// when doing Vue.use(VuePickaday,{ ... })
// and accesssible as this.$vuePickadayDefaults
var pikadayDefaults = {
  format: 'YYYY-MM-DD'
};


//<button class="uk-button uk-button-default uk-button-small">Add image</button>
//v-show="!rows[index].length"
//v-if="rows[index].length"
//v-show="!rows[index].data.length"

// Vue.component('vue-multiselect', window.VueMultiselect.default);

Vue.component('download-all', {
  template: '<a href="" class="icon-up" @click.prevent="open()"><span uk-icon="download"></span>Download all </a>',
  props: {
    urls: {
      type: Array
    }
  },
  methods: {
    open: function(){
      for (var i = 0; i < this.urls.length; i++) {
        console.log(this.urls[i]);
        var element = document.createElement('a');
        element.setAttribute('href', this.urls[i].url);
        element.setAttribute('download', this.urls[i].title);
        element.setAttribute('target', '_blank');

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
      }
    }
  }
});



Vue.component('multi-image-upload', {
  template: '<div><div><div v-for="(value, index) in rows" class="uk-margin-bottom multi-image-row"><p>${index}</p><p class="ff">${value}</p><input v-bind:name="rows[index].name" v-show="!rows[index].data.length" @change="fileChange($event, index)" type="file" /><div class="preview-img-hold" v-if="rows[index].data.length"><a href="#/" class="uk-position-absolute delete-img" uk-icon="icon: close" @click="clearInput(index);"></a><img class="full-width" v-bind:src="rows[index].data"></div></div></div></div>',
  //template: '<div><div v-for="(value, index) in rows" class="multi-image-row"><input type="file" @change="fileChange($event, index)"><a href="#/" class="uk-position-absolute delete-img" uk-icon="icon: close" @click="clearInput(index);"></a></div></div>',
  data: function () {
    return {
      rows : [
        { data: '', id: 'imgupload' + Math.ceil(Math.random()*1000), name:'' }
      ]
    }
  },
  methods: {
    fileChange: function(e, i){
      console.log('file change');
      console.log(e);
      var files = e.target.files || e.dataTransfer.files;
      var file = files[0];
      var image = new Image();
      var reader = new FileReader();
      var vm = this;
      reader.onload = function(e) {
        //vm.imageGrid[ i ].url = e.target.result;
        //imageGroup.push({ url : e.target.result });
        vm.rows[i].data = e.target.result;

      };
      vm.rows[i].name = 'fields[squareSlider][' + i + ']';
      reader.readAsDataURL(file);
      console.log( this.rows );
      //this.rows.push({ data : '', id: 'imgupload' + Math.ceil(Math.random()*1000), name: '' });
    },
    clearInput: function(i){


      console.log( 'length' + this.rows.length );

      //document.getElementById( this.rows[i].id ).value = '';
      this.rows.splice(i,1);
      //document.getElementById( this.rows[i].id ).value = '';
    }
  }
})



Vue.component('google-map', {
  props: ['single','lat','long','zoom'],
  template: '<div></div>',
  mounted: function(){
      console.log(this.lat);
      console.log(this.long);
      var map;
      var mapOptions = {
        center: new google.maps.LatLng(this.lat, this.long),
        zoom: 16,gestureHandling: 'auto',fullscreenControl: false,zoomControl: false,
        disableDoubleClickZoom: true,

        mapTypeControl: false,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        },
        scaleControl: false,
        scrollwheel: true,
        streetViewControl: false,
        draggable: true,
        clickableIcons: false,
        fullscreenControlOptions: {
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        streetViewControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        mapTypeControlOptions: {
          position: google.maps.ControlPosition.TOP_LEFT
        },

        mapTypeId: google.maps.MapTypeId.ROADMAP,

        styles: [{ featureType: "administrative", elementType: "geometry.fill", stylers: [{ color: "#f5f5f5" }] }, { featureType: "administrative", elementType: "labels.text", stylers: [{ lightness: "15" }] }, { featureType: "administrative.locality", elementType: "labels.text.fill", stylers: [{ lightness: "-11" }] }, { featureType: "administrative.neighborhood", elementType: "labels", stylers: [{ visibility: "on" }] }, { featureType: "administrative.land_parcel", elementType: "all", stylers: [{ visibility: "simplified" }] }, { featureType: "landscape", elementType: "geometry.fill", stylers: [{ color: "#f5f5f5" }, { lightness: "-1" }] }, { featureType: "landscape.natural.terrain", elementType: "geometry.fill", stylers: [{ visibility: "off" }] }, { featureType: "poi", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "poi.business", elementType: "all", stylers: [{ visibility: "off" }] }, { featureType: "road", elementType: "labels", stylers: [{ visibility: "off" }, { lightness: 20 }] }, { featureType: "road.highway", elementType: "geometry", stylers: [{ visibility: "simplified" }, { color: "#ffffff" }, { lightness: "15" }] }, { featureType: "road.highway", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "road.highway.controlled_access", elementType: "geometry", stylers: [{ visibility: "simplified" }, { lightness: "0" }, { hue: "#ff6500" }] }, { featureType: "road.highway.controlled_access", elementType: "labels", stylers: [{ visibility: "off" }] }, { featureType: "road.arterial", elementType: "geometry", stylers: [{ visibility: "simplified" }, { color: "#ffffff" }] }, { featureType: "road.arterial", elementType: "labels", stylers: [{ visibility: "on" }] }, { featureType: "road.local", elementType: "geometry", stylers: [{ visibility: "on" }] }, { featureType: "road.local", elementType: "geometry.stroke", stylers: [{ visibility: "on" }, { color: "#ffffff" }, { lightness: "0" }] }, { featureType: "road.local", elementType: "labels", stylers: [{ visibility: "off" }, { lightness: "28" }] }, { featureType: "transit", elementType: "all", stylers: [{ visibility: "off" }, { lightness: "15" }] }, { featureType: "water", elementType: "all", stylers: [{ hue: "#007bff" }, { saturation: 30 }, { lightness: 49 }] }, { featureType: "water", elementType: "geometry", stylers: [{ color: "#39c2c9" }, { saturation: "-20" }, { gamma: "1.00" }, { lightness: "46" }] }, { featureType: "water", elementType: "labels", stylers: [{ visibility: "off" }] }],

      }
      var mapElement = this.$el;
      var map = new google.maps.Map(mapElement, mapOptions);
      var _this = this;
      var marker = new google.maps.Marker({
        position: {
          lat: parseFloat(_this.lat),
          lng: parseFloat(_this.long)
        },
        map: map,
        title: 'Hello World!'
      });

  }
});

Vue.component('pickaday', {
  props: {
    value: {
      required: true
    },
    options: {
      default: function _default() {
        return {};
      }
    },

    toggle: {
      type: Boolean,
      default: false
    }
  },
  template: '<input type="text" :value="value">',

  computed: {
    opts: function opts() {
      return Object.assign({}, pikadayDefaults, this.options);
    }
  },

  watch: {
    toggle: function toggle(newValue) {
      newValue ? this.show() : this.hide();
    }
  },

  mounted: function mounted() {

    var vm = this;
    var opts = this.opts;

    // if user provided his own onSelect() callback option,
    // merge with ours into a new function
    if (opts.onSelect) {
      var old = opts.onSelect;
      opts.onSelect = function () {
        old.apply(undefined, arguments);
        vm.change(this.toString());
      };
    } else {
      opts.onSelect = function () {
        vm.change(this.toString());
      };
    }

    // set the field option, user should not define this
    opts.field = this.$el;

    // set up Pikaday
    this.pikaday = new Pikaday(opts);
  },

  // clean up after ourselves
  beforeDestroy: function beforeDestroy() {
    this.pikaday.destroy();
  },


  methods: {
    change: function change(value) {
      this.$emit('input', value);
    },
    show: function show() {
      this.pikaday.show();
    },
    hide: function hide() {
      this.pikaday.hide();
    }
  }
});

/////////////////////////////MAIN VUE//////////////////////////

var app = new Vue({

  el: '#main',
  components: {
  	Multiselect: window.VueMultiselect.default
	},
  data: {
    agreeWithTerms: false,

    //project collaborators values
    collabSelectValue: window.collabValues == undefined ? '' : window.collabValues,
    userIds: window.userIds == undefined ? '' : window.userIds,

    tagIds: window.tagIds == undefined ? '' : window.tagIds,
    tagsSelectValue: window.selectedTags == undefined ? '' : window.selectedTags,
    //tagsOption: ['tag','tag2'],
    testBG: '',
    search: '',
    multiBlockSwiperVar: '',
    recentWork: window.recentWork == undefined ? '' : window.recentWork,
    popularWork: window.popularWork == undefined ? '' : window.popularWork,
    profileImages: [],
    portalFiles: [],
    secondQuoteImage: [],
    topImages: [],
    imageGrid: [],
    quoteImage: [],
    youtubeUrl: '',
    fullWidthSlider: [],
    loaded: false,
    mobileNavOpen: false,
    headingImage: '',
    editHeadingImage: window.editHeadingImage == undefined ? false : window.editHeadingImage,
    test: '',
    w: document.documentElement.clientWidth,
    h: document.documentElement.clientHeight,
    date: window.postDate,
    squareSlider:[1,1,1,1,1,1],
    imageGrid: [],
    pass1: '',
    pass2: '',
    personalProfile: window.personalProfile == undefined ? '' : window.personalProfile,
    abstract: window.abstract == undefined ? '' : window.abstract,
    showVideo: false,
    myAccount: true,
    login: window.login == undefined ? true : window.login,
    registerForm: {
      username: '',
    }
  },
  watch: {
    // login: function(){
    //   console.log('login change');
    //   setTimeout( createUploaders, 1000 );
    // this does not work due to permission EG non loggedin users cant upload
    // },
    popularWork: {
      handler: function(){
        var _t = this;
        up();
        setTimeout(function(){
          up();
        },100);
        setTimeout(function(){
          up();
        },200);

        function up(){
          _t.multiBlockSwiperVar[1].lazy.load();
          _t.multiBlockSwiperVar[1].update();
          _t.multiBlockSwiperVar[1].scrollbar.updateSize();
          _t.lazyBg();
          console.log('laxy');
        }
      },
      deep: true
    },
    recentWork: {
      handler: function(){
        var _t = this;
        up();
        setTimeout(function(){
          up();
        },100);
        setTimeout(function(){
          up();
        },200);

        function up(){
          console.log(_t.multiBlockSwiperVar);
          if( Array.isArray( _t.multiBlockSwiperVar ) ){

            _t.multiBlockSwiperVar[0].lazy.load();
            _t.multiBlockSwiperVar[0].update();
            _t.multiBlockSwiperVar[0].scrollbar.updateSize();
            //_t.lazyBg();
          }else{
            _t.multiBlockSwiperVar.lazy.load();
            _t.multiBlockSwiperVar.update();
            _t.multiBlockSwiperVar.scrollbar.updateSize();
            //_t.lazyBg();
          }

          console.log('laxy');
        }
      },
      deep: true
    }
  },
  mounted: function(){
    window.addEventListener('resize', this.setWindowDimension);
    window.onload = this.winLoaded();
    this.lazyBg();
    this.multiBlockSwiper();
  },
  methods: {

    multiBlockSwiper: function(){
      this.multiBlockSwiperVar = new Swiper('.multi-block-swiper .swiper-container', {
        slidesPerView: 4,
        spaceBetween: 10,
        breakpoints: {
          480: {
            slidesPerView: 1
          },
          700: {
            slidesPerView: 2
          },
          1000: {
            slidesPerView: 3
          }
        },
        loop: false,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
        navigation: {
          nextEl: '.slide-next',
          prevEl: '.slide-prev',
        },
        scrollbar: {
          el: '.swiper-scrollbar',
          draggable: true,
          dragSize: 70
        },
        //lazy: true
        lazy: {
          loadPrevNext: true,
        },
      });
    },
    lazyBg: function(){


      console.log('Lazy loading images');
      var backgrounds = document.querySelectorAll('.anbg');
      for (var i = 0; i < backgrounds.length; i++) {

        new Waypoint({
          element: backgrounds[i],
          handler: function(direction) {
            //console.log('Scrolled to waypoint!')
            new bgModel(this.element);
            //console.log('ff');
          },
          offset: '110%'
        });

      }
      function bgModel(el){
        //console.log(el);
        el.style.WebkitTransition = 'background 0.5s';
        el.style.MozTransition = 'background 0.5s';
        el.style.Transition = 'background 0.5s';
        var url = el.dataset.bg;
        this.image = new Image();
        this.image.src = url;
        this.image.onload = function(){
          el.style.backgroundImage = 'url(' + url + ')';
        };
      }


    },
    getUserIds: function(ids, tags){
      var idList = [];
      var tagSplit = tags;

      for (var i = 0; i < tagSplit.length; i++) {
        idList[i] = ids[tagSplit[i]];
      }
      return idList.join(',');
    },
    getTagIds: function(ids, tags){

      var idList = [];
      var tagSplit = tags;

      for (var i = 0; i < tagSplit.length; i++) {
        idList[i] = ids[tagSplit[i]];
      }
      return idList.join(',');
    },
    searchClick: function(e){
      e.preventDefault();
      var form = document.getElementById('searchForm');
      var field = document.getElementById('searchField');
      console.log(field.value);
      if( field.value.length ){
        form.submit();
      }
    },
    loadBg: function(url){
      alert(url);
      console.log(url);
      return url;
    },
    winLoaded: function(){
      console.log('Window Loaded');
      this.loaded = true;
    },
    openMobileNav: function(e){
      e.preventDefault();
      this.mobileNavOpen = !this.mobileNavOpen;
      if(this.mobileNavOpen){
        document.body.style.overflow = "hidden";
      }else{
        document.body.style.overflow = "auto";
      }
    },
    setWindowDimension: function(){
      this.w = document.documentElement.clientWidth;
      this.h = document.documentElement.clientHeight;
    },
    uploadAssets: function(e) {
      // var files = e.target.files || e.dataTransfer.files;
      // for (var i = 0; i < files.length; i++) {
      //   var fd = new FormData();
      //   fd.append('folderId', 1);
      //
      //   fd.append("assets-upload", files[i]); // Append the file
      //
      //   var xhr = new XMLHttpRequest();
      //   xhr.open("POST", 'actions/assets/uploadFile');
      //   xhr.onload = function() {
      //     console.log( xhr.responseText );
      //    }
      //    xhr.send(fd);
      // }
    },
    showPicker: function showPicker() {
      this.$refs.picker.show();
    },
    onMultiImageChange: function(e){
      var files = e.target.files || e.dataTransfer.files;
      console.log(files);
      this.imageGrid = [];
      for (var i = 0; i < files.length; i++) {
        this.setMultiImageUrl(files[i], this.imageGrid);
      }
    },
    setMultiImageUrl: function(file, imageGroup){
      var image = new Image();
      var reader = new FileReader();
      var vm = this;
      var imageGroup = imageGroup;
      reader.onload = function(e) {
        //vm.imageGrid[ i ].url = e.target.result;
        imageGroup.push({ url : e.target.result });
      };
      reader.readAsDataURL(file);
    },
    onHeadingImageChange: function(e){
      var files = e.target.files || e.dataTransfer.files;
      if(!files.length)
        return;
      this.createHeadingImage(files[0]);
    },
    createHeadingImage: function(file){
      var image = new Image();
      var reader = new FileReader();
      var vm = this;

      reader.onload = function(e) {
        vm.headingImage = e.target.result;
      };
      reader.readAsDataURL(file);
    },
    validateInput: function(v, t){
      if(t == "username"){
        if( v.length == 0 ){
          return 0;
        }else if (/^[a-zA-Z0-9]+([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*[a-zA-Z0-9]+$/.test(v)) {
          return 1;
        }else{
          return 2;
        }

      }
    },
    openGallery: function(e,i){
      console.log( e.currentTarget );
      e.preventDefault();
      var pswpElement = document.querySelectorAll('.pswp')[0];
      // build items array
      var items = window.items;
      var element = e.currentTarget;
      // define options (if needed)
      var options = {
          index: i // start at first slide
      };
      // Initializes and opens PhotoSwipe
      var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
      gallery.init();
    }
  }
});





var homeProjectSwiper = new Swiper('.home-project-slider .swiper-container', {
  loop: true,
  autoplay: {
    delay: 4000,
  },
  navigation: {
    nextEl: '.home-project-slider-next',
    prevEl: '.home-project-slider-prev',
  },
  lazy: {
    loadPrevNext: true,
  },

  //lazy: true
});

var multiBlockSquareSwiper = new Swiper('.square-multi-block .swiper-container', {
  spaceBetween: 10,
  loop: false,
  freeMode: false,
  navigation: {
    nextEl: '.sqmb-project-slider-next',
    prevEl: '.sqmb-project-slider-prev',
  },
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  scrollbar: {
    el: '.swiper-scrollbar',
    draggable: true,
    dragSize: 70
  }
});

var swiper = new Swiper('.fullwidth-slider .swiper-container', {
    // Disable preloading of all images
    preloadImages: false,
    spaceBetween: 40,
    navigation: {
      nextEl: '.full-width-slider-next',
      prevEl: '.full-width-slider-prev'
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },

    scrollbar: {
      el: '.swiper-scrollbar',
      draggable: true,
      dragSize: 70
    },
    // Enable lazy loading
    lazy: true
});



$(document).ready(function() {
    // Toggle to show/hide replies
    $(document).on('click', 'a.comment-toggle', function(e) {
        e.preventDefault();
        $(this).parents('.comment-single:first').find('.comments-list:first').toggle();
    });
    // Toggle to show/hide reply form
    // $(document).on('click', 'a.comment-reply', function(e) {
    //     e.preventDefault();
    //     $(this).parents('.comment-single:first').find('.comment-form').toggle();
    // });
    // Handle editing
    $(document).on('click', 'a.comment-edit', function(e) {
        e.preventDefault();
        // Simply hides text and shows form
        $(this).parents('.comment-text').find('.comment-content').hide();
        $(this).parents('.comment-text').find('.edit-comment-form').show();
    });
});

function ready(fn) {
  if (document.readyState != 'loading'){
    fn();
  } else if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', fn);
  } else {
    document.attachEvent('onreadystatechange', function() {
      if (document.readyState != 'loading')
        fn();
    });
  }
}
$(document).ready(function(){
  // video pop up 

  function openModal(modal, button){
    // Setting the modal contents...
    $(modal).fadeIn("slow");

    if(button){
        var src = $(button).attr('data-video');
        $('#openVideo iframe').attr('src', src);
    }
  }

  $('.swiper-slide').click(function(e){
    e.preventDefault();
  })

  $('.js-open-video').click(function(e){
    e.preventDefault();
    openModal('#openVideo', $(this));
  });

  // When user clicks outside of modal, close it or when they click close
  $('.close-modal').click(function(){
    $('.modal').fadeOut();
    $('#openVideo iframe').attr('src', '');
  });
  $('.modal').click(function(){
    $('.modal').fadeOut();
    $('#openVideo iframe').attr('src', '');
  });
  // Prevent user clicking contents of modal and accidentally closing the modal
  $('.modal-content').click(function(e){
    e.stopPropagation();
  });

  alert('test');
});

function setHeights(){

  var heights = document.querySelectorAll('.set-height');
  var biggestHeight = 0;
  console.log(heights.length);
  for (var i = 0; i < heights.length; i++) {
    heights[i].style.height = 'auto';
  }
  for (var i = 0; i < heights.length; i++) {
    var currentHeight = heights[i].offsetHeight;
    if( currentHeight > biggestHeight ){
      biggestHeight = currentHeight;
    }
  }
  for (var i = 0; i < heights.length; i++) {
    heights[i].style.height = biggestHeight + 'px';
  }
}

window.addEventListener('resize', setHeights);


ready(function(){
  try{
    UIkit.modal('#email-confirm').show();
  }catch(e){

  }
  setHeights();

  createUploaders();

});

function createUploaders(){
  var topEl = document.getElementById('uploader');
  if(topEl){
    var topImage = uploader(topEl, 'topImages', true);
  }

  var imageGrid = document.getElementById('uploader-grid');
  if(imageGrid){
    var uploaderGrid = uploader(imageGrid, 'imageGrid', true);
  }

  var fullWidthEl = document.getElementById('uploader-full-width');
  if(fullWidthEl){
    var fullWidth = uploader(fullWidthEl, 'fullWidthSlider', true);
  }

  var portalUploadEl = document.getElementById('portal-upload');
  if(portalUploadEl){
    var portalUpload = uploader(portalUploadEl, 'portalFiles', true, true);
  }

  var quoteImageEl = document.getElementById('uploader-quote-image');
  if(quoteImageEl){
    var quoteImage = uploader(quoteImageEl, 'quoteImage', false);
  }

  var secondQuoteImageEl = document.getElementById('uploader-second-quote-image');
  if(secondQuoteImageEl){
    var secondQuoteImage = uploader(secondQuoteImageEl, 'secondQuoteImage', false);
  }

  var profileUploadEl = document.getElementById('profile-upload');
  if(profileUploadEl){
    var profileImage = uploader(profileUploadEl, 'profileImages', false);
  }
}

function uploader(el, vueVar, multiple, anyType){
  var uploader = new qq.FineUploader({
      element: el,
      sizeLimit: 8000000,
      debug: true,
      multiple: multiple,
      request: {
        endpoint: window.uploadUrl + (anyType ? 'Any' : '')
        //endpoint: '/actions/fileUpload/upload'
      },

      deleteFile: {
        enabled: true,
        endpoint: window.deleteUrl
        // method: function(){
        //   alert();
        // }
      },
      callbacks: {
        onDelete: function(id){
          console.log(id);
          // for (var i = 0; i < app[vueVar].length; i++) {
          //   if( id ==  app[vueVar][i].fineId){
          //     app[vueVar].splice(i, 1);
          //   }
          // }
          app[vueVar].splice(id, 1);
          //alert();
        },
        onComplete: function(id, name, responseJSON){
          console.log(id, name, responseJSON);
          console.log( responseJSON.assetId );
          app[vueVar].push({ fineId: id, craftId: responseJSON.assetId });
        }
      }
  });
  // addInitialFiles([
  //   { 'name' : 'f', 'uuid' : 'ef' }
  // ]),
  console.log( window[vueVar] );
  if( window[vueVar] != undefined ){
    uploader.addInitialFiles(
      //{ 'name' : 'f', 'uuid' : 'ef' }
      window[vueVar]
    );
    for (var i = 0; i < window[vueVar].length; i++) {
      var value = window[vueVar][i];
      app[vueVar].push({ fineId: value.uuid, craftId: value.uuid });
    }
    ///app[vueVar].push({ fineId: window['profileImages'].uuid, craftId: window['profileImages'].uuid });
  }

  return uploader;
}

var replyText = '';

function replyToComment(id, user, threadId){
  //background: #ffd;
  $('#reply-class').remove();
  $("<style id='reply-class' type='text/css'> #comment-" + id + "{ background: #ffd; } </style>").appendTo("head");
  console.log(id, user, threadId);

  var thread = $('#thread-' + threadId);
  thread.find('.reply-info').show().text('Replying to: ' + user);
  thread.find('.replyInput').attr('name', 'parentId').val(id);
  replyText = '<a href="#/" onmouseout="replyHoverOut()" onmouseover="replyHover(' + id + ')">@' + user.replace(/\s/g, '') + '</a>';
}

function replyHover(id){
  console.log( id );
  $("<style id='hover-reply' type='text/css'> #comment-" + id + "{ background: #ffd; } </style>").appendTo("head");
}
function replyHoverOut(){
  $('#hover-reply').remove();
}

$('.toggle-comments').click(function(e){
  e.preventDefault();
  // console.log( $(this).attr('toggled') );
  // if( $(this).attr('toggled') == undefined || $(this).attr('toggled') == 'open' ){
  //   $(this).html( '<span uk-icon="close"></span> Close comments' );
  //   $(this).attr('toggled', 'close');
  // }else{
  //   $(this).html( '<span uk-icon="comment"></span> Leave a comment' );
  //   $(this).attr('toggled', 'open');
  // }

  $(this).parent().parent().parent().find('.comments-box').slideToggle('fast');
});


$('.comment-form').submit(function(e){
  e.preventDefault();
  var textArea = $(this).find('textarea');

  textArea.val( replyText + ' ' + textArea.val() );

  var data = $(this).serialize();
  $.ajax({
    method: 'POST',
    url: data.action,
    data: data,
    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
  })
  .success(function(data) {
    console.log('success', data);
  })
  .error(function(data) {
    console.log('error', data);
  });
  $(this).find('textarea').blur().val('');
  var _this = $(this);
  setTimeout(function(){
    refreshComments( _this.attr('data-entryid') );
  },500);

  replyText = '';
  $('.reply-info').hide();
  $('#reply-class').remove();
  $('.replyInput').attr('name', '').val('');
});


function upVote(event, url){
  event.preventDefault();
  $.ajax({
    method: 'GET',
    url: url
  });
}
function downVote(event, url){
  event.preventDefault();
  $.ajax({
    method: 'GET',
    url: url
  });
}

function deleteComment(event, url){
  event.preventDefault();
  $.ajax({
    method: 'GET',
    url: url
  });
}

function refreshAll(){
  setInterval(function(){
    $('.thread').each(function(){
      var id = $(this).find('.comments-list').attr('data-ajax-id');
      console.log(id);
      refreshComments(id);
    });
  },5000);
}
if( $('.thread').length ){
  refreshAll();
}
$('.js-register-form').submit(function(event){
  if ($('#bear').val() != ''){
      event.preventDefault();
  }
});
function refreshComments(id){
  $.ajax({
    method: 'GET',
    url: window.siteUrl + '/portal/ajax?entryId=' + id
  })
  .success(function(data) {
    $("[data-ajax-id=" + id + "]").html(data);

    try {
      var commentAmount = data.match(/comment-single/gi).length;
      $("[data-ajax-id=" + id + "]").parent().parent().parent().parent().find('.toggle-comments').html('<span uk-icon="comment"></span> Comments (' + commentAmount + ')');
    } catch (e) {

    } finally {

    }

    //$("[data-ajax-id=" + id + "]").html('');
    //console.log('success', data);
  })
  .error(function(data) {
    console.log('error', data);
  });
}



// function lazyBg(){
//   console.log('Lazy loading images');
//   var backgrounds = document.querySelectorAll('.anbg');
//   for (var i = 0; i < backgrounds.length; i++) {
//
//     new Waypoint({
//       element: backgrounds[i],
//       handler: function(direction) {
//         //console.log('Scrolled to waypoint!')
//         new bgModel(this.element);
//         //console.log('ff');
//       },
//       offset: '110%'
//     });
//
//   }
//   function bgModel(el){
//     //console.log(el);
//     el.style.WebkitTransition = 'background 0.5s';
//     el.style.MozTransition = 'background 0.5s';
//     el.style.Transition = 'background 0.5s';
//     var url = el.dataset.bg;
//     this.image = new Image();
//     this.image.src = url;
//     this.image.onload = function(){
//       el.style.backgroundImage = 'url(' + url + ')';
//     };
//   }
// }
//lazyBg();
