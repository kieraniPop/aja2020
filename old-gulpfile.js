var gulp = require('gulp');
var uglify = require('gulp-uglify');
var pump = require('pump');
var rename = require("gulp-rename");
var babel = require('gulp-babel');
var pug = require('gulp-pug');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');

var jade = require('gulp-jade');

var browserify = require('gulp-browserify');

 
gulp.task('compressjs', function (cb) {
  pump([
        gulp.src('src/js/**/*.js'),
        babel({
            presets: ['env']
        }),
        uglify(),
        //rename({ suffix: '.min' }),
        gulp.dest('res/js')
    ],
    cb
  );
});

gulp.task('views', function buildHTML() {
    return gulp.src('craft/templates/**/*.pug')
    .pipe(pug({
        pretty: false
        // Your options in here.
    })).pipe( gulp.dest('craft/templates/') )
});

gulp.task('templates', function() {
gulp.src('craft/templates/**/*.jade')
    .pipe(jade({
        pretty: false
    }))
    .pipe(gulp.dest('craft/templates/'))
});

gulp.task('sass', function () {
    return gulp.src('res/css/scss/style22.scss')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
      .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
      .pipe(rename({ suffix: '.min' }))
      .pipe(gulp.dest('res/css'));
});
   

gulp.task('watch', function () {
    gulp.watch('res/css/scss/style22.scss', ['sass']);
    gulp.watch('res/js/**/*.js', ['compressjs']);
    gulp.watch('craft/templates/**/*.pug', ['views']);
    gulp.watch('craft/templates/**/*.jade', ['templates']);
});

// Basic usage 
gulp.task('scripts', function() {
    // Single entry point to browserify 
    gulp.src('src/js/main.js')
        .pipe(browserify({
        //   insertGlobals : true,
        //   debug : !gulp.env.production
        }))
        //.pipe(uglify())
        .pipe(gulp.dest('res/js/'))
});


gulp.task('default', ['compressjs', 'views', 'sass', 'watch' ,'templates']);



// function scripts() {
//     return gulp.src('src/js/main.js')
//     .pipe(browserify({
//     //   insertGlobals : true,
//     //   debug : !gulp.env.production
//     }))
//     .pipe(gulp.dest('res/js/'))
// }
